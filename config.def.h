/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;      /* -b  option; if 0, dmenu appears at bottom */

static int user_bh = 0;     /* add an defined amount of pixels to the bar height */

/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Monaco:pixelsize=16:antialias=true:autohint=true",
};

/* dwm colors */
static const char col_gray1[]   = "#06030f";
static const char col_gray2[]   = "#9a8eaa";
static const char col_gray3[]   = "#9460d6";
static const char col_gray4[]   = "#080412";
static const char col_accent[]  = "#b073ff";

static const char *colors[SchemeLast][2] = {
	/*		 fg         bg       */
	[SchemeNorm] = { col_gray3, col_gray1  },
	[SchemeSel]  = { col_gray4, col_accent },
	[SchemeOut]  = { "#000000", "#00ffff"  },
};

static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 1;

/* put dmenu at this x offset */
static int dmx = 10;

/* put dmenu at this y offset (measured from the bottom if topbar is 0) */
static int dmy = 10;

/* make dmenu this wide */
static unsigned int dmw = 1898;
